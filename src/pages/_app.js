import '../styles/globals.css'
import './login/login.css'
import { ThemeContextProvider } from '../contexts/ThemeContext'

function MyApp({ Component, pageProps }) {
  return <ThemeContextProvider>
    <Component {...pageProps} />
  </ThemeContextProvider>
}

export default MyApp
