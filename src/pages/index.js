// import Head from 'next/head'
// import Image from 'next/image'
// import styles from '../styles/Home.module.css'
import React, { useContext, useEffect } from "react";

import Header from '../components/header/header'
import { ThemeContext } from '../contexts/ThemeContext'

export default function Home() {

  const { loggedusername, setLoggedusername } = useContext(ThemeContext)

  useEffect(() => {
    // setLoggedusername('sas')
    console.log(loggedusername)
  });

  return (
    <div className="">
      <Header />
      hello world {loggedusername}
    </div>
  )
}
