import React, { createContext, useState } from "react";

export const ThemeContext = createContext();

export const ThemeContextProvider = props => {
    const [loggedusername, setLoggedusername] = useState('suman')

    return (
        <ThemeContext.Provider value={loggedusername, setLoggedusername}>
            {props.children}
        </ThemeContext.Provider>
    );
}